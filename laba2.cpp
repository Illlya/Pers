
#include <iostream>
using namespace std;


struct Node 
{
	int data;
	Node* next;
};
void insert(Node *first, int value);
bool remove(Node *first, int value);
void print(Node *first);

int main()
{
	Node *node0 = new Node{ 0,nullptr 
	};
	print(node0);
	insert(node0, 1);
	print(node0);
	if (remove(node0, 1) == 1) 
	{
		cout << "deleted " << endl;
	}
	print(node0);
  }
void insert(Node *first, int value) 
{
	if (first == nullptr)
	{
		return;
	}
	Node *busy = first;
	while (busy->next != nullptr) 
	{
		busy = busy->next;
	}
	busy->next = new Node{ value,nullptr };
}
bool remove(Node *first, int value) {
	if (first == nullptr) {
		
		return 0;
	}
	Node *busy = first;
	while (busy != nullptr) {
		if (busy->next->data == value) {
			delete busy->next;
			busy->next = nullptr;
			return 1;
		}
		busy = busy->next;
	}
	return 0;
}
void print(Node *first) 
{
	if (first == nullptr) 
	{
	return;	
	}
	Node *busy = first;
	while (busy != nullptr) 
	{
		cout << busy->data << endl;
		busy = busy->next;
	
	}
}
